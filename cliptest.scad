include <BOSL/constants.scad>
use <BOSL/transforms.scad>
$fn = 200;

//========dimensions==============

w = 8;		// width
th = 2.2;		// thickness

//ov = 0.8;		// overhang

l = 30;		// length of testclip
a = 60;		// angle

module arm(ov1){
union(){
	square([th,w]);
	translate([-th - 2*ov1, w, 0]) square([2*th + 2*ov1,th]);
	//translate([-th - ov, w, 0]) circle(ov);
	translate([-th - 2*ov1, w - ov1, 0]) square(2*ov1);
}
}

module sketch(ov0){
union(){
arm(ov0);
mirror([-1,1,0]) arm(ov0);
}
}

difference(){
for (ov =[0.8:0.1:1.2]) xmove(200*(ov-0.8)) rotate([0,90 - a,0]) linear_extrude(l) sketch(ov);;
zmove(-80) ymove(-40) cube(80);
}
