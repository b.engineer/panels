include <BOSL/constants.scad>
use <BOSL/transforms.scad>
use <BOSL/shapes.scad>
$fn = 200;

//========dimensions==============

w = 8;		// width
th = 1;	// thickness

rth = 2.5;		// rail thickness
ov = 2;

l = 20;		// length of testclip
a = 30;		// angle

module arm(){
translate([th + rth,th + rth,0])
union(){
	square([th,w]);
	translate([-th - rth, w, 0]) square([2*th + rth,th]);
	//translate([-th - ov, w, 0]) circle(ov);
	//translate([-th - 2*ov, w - ov, 0]) square(2*ov);
}
}

module clip(){
rotate([0,a,0])
linear_extrude(l)
union(){
arm();
mirror([-1,1,0]) arm();
}
}

module bb(){
rotate([0,a,0])
union(){
move(x=-1, y=-1) cube([2*th + rth + 1,w + 2*th + rth + 1,l]);
mirror([-1,1,0])
move(x=-1, y=-1) cube([2*th + rth + 1,w + 2*th + rth + 1,l]);
}
}

clip();
%bb();
