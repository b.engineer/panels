include <BOSL/constants.scad>
use <BOSL/transforms.scad>
use <BOSL/shapes.scad>
use <clip.scad>

$fn = 200;
a = 60;
x = 80;
y = 100;
z = tan(a) * y/2;
th = 1;
th0 = th / cos(90 - a);
hx = 20;

//positve stop
st_l = 10;
st_th = 3;

module prism(l, w, h){
translate([-l/2,-w/2,0]) polyhedron(
points=[[0,0,0], [l,0,0], [l,w,0], [0,w,0], [0,w/2,h], [l,w/2,h]],
faces=[[0,1,2,3],[5,4,3,2],[0,4,5,1],[0,3,4],[5,2,1]]
);
}

difference(){
union(){
	difference(){
		prism(x,y,z);
		prism(x-2*th0,y-2*th0,z-2*th0);



		translate([x/2,-y/2,0]) rotate([0,0,90]) bb();
		mirror([1,0,0]) translate([x/2,-y/2,0]) rotate([0,0,90]) bb();
		mirror([0,1,0]) translate([x/2,-y/2,0]) rotate([0,0,90]) bb();
		translate(-[x/2,-y/2,0]) rotate([0,0,270]) bb();
	}

	//clips
	translate([x/2,-y/2,0]) rotate([0,0,90]) clip();
	mirror([1,0,0]) translate([x/2,-y/2,0]) rotate([0,0,90]) clip();
	mirror([0,1,0]) translate([x/2,-y/2,0]) rotate([0,0,90]) clip();
	translate(-[x/2,-y/2,0]) rotate([0,0,270]) clip();

	//positive stop
	translate([x/2 - st_th/2,-y/2,0]) xrot(-90+a) zmove(50) ymove(st_l/2) upcube([st_th,st_l,st_l]);
	translate([-x/2 + st_th/2,-y/2,0]) xrot(-90+a) zmove(50) ymove(st_l/2) upcube([st_th,st_l,st_l]);

	translate([x/2 - st_th/2,y/2,0]) xrot(90-a) zmove(50) ymove(-st_l/2) upcube([st_th,st_l,st_l]);
	translate([-x/2 + st_th/2,y/2,0]) xrot(90-a) zmove(50) ymove(-st_l/2) upcube([st_th,st_l,st_l]);

	//edge holder
	move(x=x/2 + hx/2 - 1.5 * th0, z=z/2)difference(){
		prism(hx,y/2,z/2);
		prism(hx-3*th0,y/2-3*th0,z/2-2*th0);
		zmove(20) yrot(-45) downcube(100);
	}

}
translate([0,0,-z/2]) cube([x,y,z],center=true);
}
